<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class SetupTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email')->unique();
            $table->string('password');
            $table->boolean('banned')->default(0);
            $table->boolean('email_confirmed')->default(0);
            $table->rememberToken();
            $table->timestamps();
        });

        Schema::create('publishers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->string('url');
            $table->timestamp('last_crawled')->nullable();
            $table->timestamps();
        });

        Schema::create('brands', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->string('domain');
            $table->binary('logo')->nullable();
            $table->string('facebook_handle')->nullable();
            $table->string('twitter_handle')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('publisher_brands_pivot', function (Blueprint $table) {
            $table->integer('publisher_id')->unsigned()->nullable();
            $table->foreign('publisher_id')->references('id')->on('publishers');
            $table->integer('brand_id')->unsigned()->nullable();
            $table->foreign('brand_id')->references('id')->on('brands');
        });

        Schema::create('ads', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('publisher_id')->unsigned()->nullable();
            $table->foreign('publisher_id')->references('id')->on('publishers');
            $table->integer('brand_id')->unsigned()->nullable();
            $table->foreign('brand_id')->references('id')->on('brands');
            $table->string('url');
            $table->binary('screenshot')->nullable();
            $table->timestamps();
        });

        Schema::create('campaigns', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('publisher_id')->unsigned()->nullable();
            $table->foreign('publisher_id')->references('id')->on('publishers');
            $table->integer('brand_id')->unsigned()->nullable();
            $table->foreign('brand_id')->references('id')->on('brands');
            $table->string('title');
            $table->string('description')->nullable();
            $table->binary('image');
            $table->timestamps();
        });

//        DB::select(DB::raw("ALTER TABLE users AUTO_INCREMENT = ". mt_rand(100000, 999999) .""));
//        DB::select(DB::raw("ALTER TABLE publishers AUTO_INCREMENT = ". mt_rand(100000, 999999) .""));
//        DB::select(DB::raw("ALTER TABLE brands AUTO_INCREMENT = ". mt_rand(100000, 999999) .""));
//        DB::select(DB::raw("ALTER TABLE ads AUTO_INCREMENT = ". mt_rand(100000, 999999) .""));
//        DB::select(DB::raw("ALTER TABLE campaigns AUTO_INCREMENT = ". mt_rand(100000, 999999) .""));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */ 
    public function down()
    {
        Schema::drop('campaigns');
        Schema::drop('ads');
        Schema::drop('publisher_brands_pivot');
        Schema::drop('brands');
        Schema::drop('publishers');
        Schema::drop('users');
    }
}
