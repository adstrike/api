## AdStrike API

This api is built with Laravel 5.1

API Docs: Click **File > Import URL** in [swagger ui](http://editor.swagger.io/) and then paste this url:

[https://bitbucket.org/adstrike/api/raw/bcd8f60206d66b8416414a7823551550f465b069/api/swagger.yaml](https://bitbucket.org/adstrike/api/raw/bcd8f60206d66b8416414a7823551550f465b069/api/swagger.yaml)