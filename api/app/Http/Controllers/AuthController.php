<?php

namespace App\Http\Controllers;

use App\Contracts\UserInterface;
use App\Transformers\UserTransformer;

use Input;
use JWTAuth;
use Auth;

class AuthController extends ApiController
{
    /**
     * Create a new user controller instance.
     */
    public function __construct(UserInterface $userRepo, UserTransformer $userTransformer)
    {
        $this->userRepo = $userRepo;
        $this->userTransformer = $userTransformer;
    }

    /**
     * Authentication
     *
     * Login with a username and password and retrieve a Bearer token from the api
     *
     * @return User
     */
    public function authenticate()
    {
        // Get credentials from the request
        $credentials = Input::only('email', 'password');

        // attempt to verify the credentials and create a token for the user
        $auth = Auth::attempt($credentials);

        if (!$auth)
        {
            return $this->response->array(['error' => 'invalid_credentials'])->statusCode(422);
        }

        $user = Auth::user();
        $token = JWTAuth::fromUser($user);

        if (!$token)
        {
            //  Cannot create token
            return $this->response->array(['error' => 'could_not_create_token'])->statusCode(500);
        }

        $ip = request()->ip();

        $update_ip = $this->userRepo->updateLastLoginIp($user->id, $ip);

        $user_transformed = $this->userTransformer->transform($user);

        // all good so return the token
        return $this->response->array([ 'token' => $token, 'user_id' => (int) $user->id, 'user' => $user_transformed ])->statusCode(200);
    }
}


