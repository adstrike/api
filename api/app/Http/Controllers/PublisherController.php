<?php

namespace App\Http\Controllers;

use App\Contracts\PublisherDBContract;
use App\Transformers\PublisherTransformer;
use Illuminate\Http\Request;

class PublisherController extends ApiController
{
    private $publisherDB;

    /**
     * PublisherController constructor.
     * @param PublisherDBContract $publisherDB
     */
    public function __construct(PublisherDBContract $publisherDB)
    {
        parent::__construct();

        $this->publisherDB = $publisherDB;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function get(Request $request)
    {
        $ids = $request->input('ids', null);
        $ids = $ids ? explode(',', $ids) : null;
        $items = $this->publisherDB->getPublishers($ids);

        if (!$items)
        {
            return $this->response->errorNotFound();
        }

        return $this->response->collection($items, new PublisherTransformer);
    }
}