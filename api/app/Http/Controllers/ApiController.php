<?php

namespace App\Http\Controllers;

use Dingo\Api\Routing\Helpers;
use JWTAuth;
use Exception;

/**
 * Class ApiController
 * @package App\Http\Controllers
 * @property mixed response
 */
class ApiController extends Controller
{
    use Helpers;

    /**
     * @var null
     */
    protected $user;

    /**
     * ApiController constructor.
     */
    function __construct()
    {
        $this->user = null;

//        try
//        {
//            $this->user = JWTAuth::parseToken()->authenticate();
//        }
//
//        catch (Exception $e)
//        {
//            $this->user = null;
//        }
    }
}
