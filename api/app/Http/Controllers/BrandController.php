<?php

namespace App\Http\Controllers;

use App\Contracts\BrandDBContract;
use App\Libraries\BrandHelper;
use App\Transformers\BrandTransformer;
use Illuminate\Http\Request;

class BrandController extends ApiController
{
    private $brandDB;
    private $brandHelper;

    /**
     * PublisherController constructor.
     * @param BrandDBContract $brandDB
     * @param BrandHelper $brandHelper
     */
    public function __construct(BrandDBContract $brandDB, BrandHelper $brandHelper)
    {
        parent::__construct();

        $this->brandDB = $brandDB;
        $this->brandHelper = $brandHelper;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function get(Request $request)
    {
        $ids = $request->input('ids', null);
        $ids = $ids ? explode(',', $ids) : null;
        $items = $this->brandDB->getBrands($ids);

        if (!$items)
        {
            return $this->response->errorNotFound();
        }

        return $this->response->collection($items, new BrandTransformer);
    }
}