<?php

namespace App\Http\Controllers;

use App\Contracts\AdDBContract;
use App\Contracts\BrandDBContract;
use App\Contracts\PublisherDBContract;
use App\Libraries\IngestionHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class IngestionController extends ApiController
{
    private $publisherDB;
    private $brandDB;
    private $adDB;
    private $ingestionHelper;

    /**
     * IngestionController constructor.
     * @param PublisherDBContract $publisherDB
     * @param BrandDBContract $brandDB
     * @param AdDBContract $adDB
     * @param IngestionHelper $ingestionHelper
     */
    public function __construct(PublisherDBContract $publisherDB, BrandDBContract $brandDB, AdDBContract $adDB, IngestionHelper $ingestionHelper)
    {
        parent::__construct();

        $this->publisherDB = $publisherDB;
        $this->brandDB = $brandDB;
        $this->adDB = $adDB;
        $this->ingestionHelper = $ingestionHelper;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function post(Request $request)
    {
        $key = $request->input('auth', false);

        if (!$key || $key != env('INGEST_KEY'))
        {
            return $this->response->errorUnauthorized();
        }

        if ($request->hasFile('file'))
        {
            if (!$request->file('file')->isValid())
            {
                return $this->response->errorBadRequest();
            }

            $path = $request->file('file')->getRealPath();

            $publishers = $this->ingestionHelper->parseCSV($path);
            
            foreach ($publishers as $publisher_url_md5 => $p)
            {
                $publisher = $this->publisherDB->postPublisher($p['publisher_url']);

                foreach ($p['brands'] as $brand_domain_md5 => $b)
                {
                    $brand = $this->brandDB->postBrand($b);
                    $this->brandDB->associateBrandToPublisher($brand->id, $publisher->id);
                    $this->adDB->postAd($publisher, $brand, $b['brand_url'], ''); //$b['screenshot']);
                }
            }

            return $this->response->noContent();
        }
        else
        {
            return $this->response->errorBadRequest();
        }
    }
}