<?php

$api = app('api.router');

$api->version('v1', function($api)
{
    $api->get('health_check', function() {
        return '';
    });

    //  PUBLISHERS
    $api->group(['prefix' => 'publishers'], function($api)
    {
        $api->get('/', [ 'uses' => 'App\Http\Controllers\PublisherController@get' ]);
    });

    //  BRANDS
    $api->group(['prefix' => 'brands'], function($api)
    {
        $api->get('/', [ 'uses' => 'App\Http\Controllers\BrandController@get' ]);
    });

    //  INGESTION
    $api->group(['prefix' => 'ingest'], function($api)
    {
        $api->post('/', [ 'uses' => 'App\Http\Controllers\IngestionController@post' ]);
    });
});
