<?php

namespace App\Http\Middleware;

use Closure;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Middleware\GetUserFromToken as JwtGetUserFromToken;

class GetUserFromToken extends JwtGetUserFromToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$token = $this->auth->setRequest($request)->getToken())
        {
            return response()->json(['status' => 'error', 'data' => 'token_not_provided' ], 401);
        }

        try
        {
            $user = $this->auth->authenticate($token);
        }
        catch (TokenExpiredException $e)
        {
            return response()->json(['status' => 'error', 'data' => 'token_expired' ], 401);
        }
        catch (JWTException $e)
        {
            return response()->json(['status' => 'error', 'data' => 'token_invalid' ], 401);
        }

        if (!$user)
        {
            return response()->json(['status' => 'error', 'data' => 'user_not_found' ], 404);
        }

        $this->events->fire('tymon.jwt.valid', $user);

        return $next($request);
    }
}
