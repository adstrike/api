<?php

namespace App\Contracts;

/**
 * @package App\Contracts
 */
interface BrandDBContract
{
    /**
     * @param $ids
     * @return mixed
     */
    public function getBrands($ids);

    /**
     * @param $id
     * @return mixed
     */
    public function getBrand($id);

    /**
     * @param $brand_array
     * @return mixed
     */
    public function postBrand($brand_array);

    /**
     * @param $brand_id
     * @param $publisher_id
     * @return mixed
     */
    public function associateBrandToPublisher($brand_id, $publisher_id);
}