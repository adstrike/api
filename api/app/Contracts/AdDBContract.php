<?php

namespace App\Contracts;

/**
 * @package App\Contracts
 */
interface AdDBContract
{
    /**
     * @param $publisher
     * @param $brand
     * @param $url
     * @param $screenshot
     * @return mixed
     */
    public function postAd($publisher, $brand, $url, $screenshot);
}