<?php

namespace App\Contracts;

/**
 * @package App\Contracts
 */
interface PublisherDBContract
{
    /**
     * @param $ids
     * @return mixed
     */
    public function getPublishers($ids);

    /**
     * @param $id
     * @return mixed
     */
    public function getPublisher($id);

    /**
     * @param $publisher_url
     * @return mixed
     */
    public function postPublisher($publisher_url);
}