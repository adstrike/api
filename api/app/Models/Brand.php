<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Brand extends Model
{
    protected $table = 'brands';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes that are dates.
     *
     * @var array
     */
    protected $dates = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function publishers()
    {
        return $this->belongsToMany('App\Models\Publisher', 'publisher_brands_pivot', 'brand_id', 'publisher_id');
    }

    public function ads()
    {
        return $this->hasMany('App\Models\Ad');
    }
}
