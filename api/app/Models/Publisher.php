<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Publisher extends Model
{
    protected $table = 'publishers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes that are dates.
     *
     * @var array
     */
    protected $dates = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function brands()
    {
        return $this->belongsToMany('App\Models\Brand', 'publisher_brands_pivot', 'publisher_id', 'brand_id');
    }

    public function ads()
    {
        return $this->hasMany('App\Models\Ad');
    }
}
