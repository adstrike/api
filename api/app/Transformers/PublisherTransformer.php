<?php

namespace App\Transformers;

use App\Models\Publisher;
use League\Fractal\TransformerAbstract;

class PublisherTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'brands',
        'ads'
    ];

    public function transform(Publisher $publisher)
    {
        return [
            'id' => (int) $publisher->id,
            'title' => $publisher->title,
            'url' => $publisher->url
        ];
    }

    public function includeBrands(Publisher $publisher)
    {
        $brand = $publisher->brands;

        return $this->collection($brand, new BrandTransformer);
    }

    public function includeAds(Publisher $publisher)
    {
        $ads = $publisher->ads;

        return $this->collection($ads, new AdTransformer);
    }
}