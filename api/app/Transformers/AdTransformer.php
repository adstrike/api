<?php

namespace App\Transformers;

use App\Models\Ad;
use League\Fractal\TransformerAbstract;

class AdTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'publisher',
        'brand'
    ];

    public function transform(Ad $ad)
    {
        return [
            'id' => (int) $ad->id,
            'url' => $ad->url,
            'screenshot' => $ad->screenshot
        ];
    }

    public function includePublisher(Ad $ad)
    {
        $publisher = $ad->publisher;

        return $this->item($publisher, new PublisherTransformer);
    }

    public function includeBrand(Ad $ad)
    {
        $brand = $ad->brand;

        return $this->item($brand, new BrandTransformer);
    }
}