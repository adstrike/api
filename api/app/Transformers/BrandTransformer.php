<?php

namespace App\Transformers;

use App\Models\Brand;
use League\Fractal\TransformerAbstract;

class BrandTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'publishers',
        'ads'
    ];

    public function transform(Brand $brand)
    {
        return [
            'id' => (int) $brand->id,
            'title' => $brand->title,
            'domain' => $brand->domain
        ];
    }

    public function includePublishers(Brand $brand)
    {
        $publishers = $brand->publishers;

        return $this->collection($publishers, new PublisherTransformer);
    }

    public function includeAds(Brand $brand)
    {
        $ads = $brand->ads;

        return $this->collection($ads, new AdTransformer);
    }
}