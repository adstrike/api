<?php

namespace App\Repositories;

use App\Contracts\PublisherDBContract;
use App\Models\Publisher;

/**
 * Class PublisherMysqlRepository
 * @package App\Repositories
 */
class PublisherMysqlRepository implements PublisherDBContract
{
    /**
     * @param $ids
     * @return mixed
     */
    public function getPublishers($ids)
    {
        $items = $ids ? Publisher::whereIn('id', $ids)->get() : Publisher::all();

        return $items;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getPublisher($id)
    {
        $record = Publisher::find($id);

        return $record;
    }

    /**
     * @param $publisher_url
     * @return mixed
     */
    public function postPublisher($publisher_url)
    {
        $pub = Publisher::where('url', $publisher_url)->first();

        if (!$pub)
        {
            $pub = new Publisher;
            $pub->url = $publisher_url;
            $pub->save();
        }

        return $pub;
    }
}