<?php

namespace App\Repositories;

use App\Contracts\BrandDBContract;
use App\Models\Brand;

/**
 * Class BrandMysqlRepository
 * @package App\Repositories
 */
class BrandMysqlRepository implements BrandDBContract
{
    /**
     * @param $ids
     * @return mixed
     */
    public function getBrands($ids)
    {
        $items = $ids ? Brand::whereIn('id', $ids)->get() : Brand::all();

        return $items;
    }

    /** 
     * @param $id
     * @return mixed
     */
    public function getBrand($id)
    {
        $record = Brand::find($id);

        return $record;
    }

    /**
     * @param $brand_array
     * @return mixed
     */
    public function postBrand($brand_array)
    {
        $brand = Brand::where('domain', $brand_array['brand_domain'])->first();

        if (!$brand)
        {
            $brand = new Brand;
            $brand->domain = $brand_array['brand_domain'];
            $brand->save();
        }

        return $brand;
    }

    /**
     * @param $brand_id
     * @param $publisher_id
     * @return mixed
     */
    public function associateBrandToPublisher($brand_id, $publisher_id)
    {
        $brand = Brand::find($brand_id);
        $brand->publishers()->attach($publisher_id);

        return $brand;
    }
}