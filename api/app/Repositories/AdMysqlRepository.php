<?php

namespace App\Repositories;

use App\Contracts\AdDBContract;
use App\Models\Ad;

/**
 * Class BrandMysqlRepository
 * @package App\Repositories
 */
class AdMysqlRepository implements AdDBContract
{
    /**
     * @param $publisher
     * @param $brand
     * @param $url
     * @param $screenshot
     * @return mixed
     */
    public function postAd($publisher, $brand, $url, $screenshot)
    {
        $ad = new Ad;
        $ad->publisher_id = $publisher->id;
        $ad->brand_id = $brand->id;
        $ad->url = $url;
        $ad->screenshot = $screenshot;
        $ad->save();

        return $ad;
    }
}