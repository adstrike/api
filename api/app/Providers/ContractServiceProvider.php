<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ContractServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('App\Contracts\UserDBContract', 'App\Repositories\UserMysqlRepository');
        $this->app->bind('App\Contracts\BrandDBContract', 'App\Repositories\BrandMysqlRepository');
        $this->app->bind('App\Contracts\PublisherDBContract', 'App\Repositories\PublisherMysqlRepository');
        $this->app->bind('App\Contracts\AdDBContract', 'App\Repositories\AdMysqlRepository');
    }
}