<?php

namespace App\Libraries;

use League\Csv\Reader;

class IngestionHelper
{
    /**
     * IngestionHelper constructor.
     */
    function __construct()
    {

    }

    /**
     * @param $csv_path
     * @return array
     */
    public function parseCSV($csv_path)
    {
        if (!ini_get("auto_detect_line_endings"))
        {
            ini_set("auto_detect_line_endings", '1');
        }

        $publishers = [];
        $reader = Reader::createFromPath($csv_path);

        foreach ($reader as $index => $csv_row)
        {
            list($publisher_url, $brand_url, $brand_domain, $screen_shot) = $csv_row;

            $publisher_url_md5 = md5($publisher_url);
            $brand_domain_md5 = md5($brand_domain);

            if (!array_key_exists($publisher_url_md5, $publishers))
            {
                $publishers[$publisher_url_md5] = [
                    'publisher_url' => $publisher_url,
                    'brands' => []
                ];
            }

            if (!array_key_exists($brand_domain_md5, $publishers[$publisher_url_md5]['brands']))
            {
                $publishers[$publisher_url_md5]['brands'][$brand_domain_md5] = [
                    'brand_domain' => $brand_domain,
                    'brand_url' => $brand_url,
                    'screenshot' => $screen_shot
                ];
            }
        }

//        dd($publishers);

        return $publishers;
    }
}