# STEPS TO RUN
# 0) upgrade to Python 3? 
# 1) setup Selenium -- http://seleniumhq.github.io/selenium/docs/api/py/index.html
# 2) put Selenium webdriver in same folder as script
#    (Chrome for MAC) - https://chromedriver.storage.googleapis.com/index.html?path=2.26/
#
# KNOWN ISSUES
# images offset incorrectly with retina screens
# browsers keep jumping in front of other windows, potentially disturbing users' other work


from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from PIL import Image
import base64
import time
import datetime
import string
import random
import csv
import sys
import os
import re
import pycurl

def fullpage_screenshot(b, file):
		print("Starting chrome full page screenshot workaround ...")
		total_width = b.execute_script("return document.body.offsetWidth")
		total_height = b.execute_script("return document.body.parentNode.scrollHeight")
		viewport_width = b.execute_script("return document.body.clientWidth")
		viewport_height = b.execute_script("return window.innerHeight")

#		trying to fix screen capture on retina screens 
#		print('total_width =' + str(total_width))
#		print('total_height =' + str(total_height))
#		print('viewport_width =' + str(viewport_width))
#		print('viewport_height =' + str(viewport_height))


		print("Total: ({0}, {1}), Viewport: ({2},{3})".format(total_width, total_height,viewport_width,viewport_height))
		rectangles = []
		i = 0
		while i < total_height:
			ii = 0
			top_height = i + viewport_height
			if top_height > total_height:
				top_height = total_height
			while ii < total_width:
				top_width = ii + viewport_width
				if top_width > total_width:
					top_width = total_width
				print("Appending rectangle ({0},{1},{2},{3})".format(ii, i, top_width, top_height))
				rectangles.append((ii, i, top_width,top_height))
				ii = ii + viewport_width
			i = i + viewport_height
		stitched_image = Image.new('RGB', (total_width, total_height))
		previous = None
		part = 0
		for rectangle in rectangles:
			if not previous is None:
				b.execute_script("window.scrollTo({0}, {1})".format(500, rectangle[1]))
				print("Scrolled To ({0},{1})".format(500, rectangle[1]))
				time.sleep(0.2)
			file_name = "part_{0}.png".format(part)
			print("Capturing {0} ...".format(file_name))
			b.get_screenshot_as_file(file_name)
			screenshot = Image.open(file_name)
			if rectangle[1] + viewport_height > total_height:
				offset = (rectangle[0], total_height - viewport_height)
			else:
				offset = (rectangle[0], rectangle[1])
			print("Adding to stitched image with offset ({0}, {1})".format(offset[0],offset[1]))
			stitched_image.paste(screenshot, offset)
			del screenshot
			os.remove(file_name)
			part = part + 1
			previous = rectangle
		stitched_image.save(file,optimize=True,quality=45)
		print("Finishing chrome full page screenshot workaround...")
		return True

def grabScreenshot(b):
	# Get the actual page dimensions using javascript
	pageWidth  = b.execute_script("return Math.max(document.body.scrollWidth, document.body.offsetWidth, document.documentElement.clientWidth, document.documentElement.scrollWidth, document.documentElement.offsetWidth);")
	pageHeight = b.execute_script("return Math.max(document.body.scrollHeight, document.body.offsetHeight, document.documentElement.clientHeight, document.documentElement.scrollHeight, document.documentElement.offsetHeight);")
	b.set_window_size(pageWidth, pageHeight) # set screensize to maximum offered by site... but not to exceed client's physical screensize
	return b.get_screenshot_as_base64()

def findAndParseAdsAndImages(b, path_to_save_files):
	# try:
	fullpage_screenshot(b, path_to_save_files+'/'+'screenshot.png')
	print("finish full screenshot and saved it to screenshot.png")
	turls, tbrands = grabBrandNamesOfTaboolaAdvertisers(b)
	gurls, gbrands, gimage_names = grabDomainsAndImagesOfGoogleAdvertisers(b, path_to_save_files)
	urls = gurls #turls + gurls
	brands = gbrands #tbrands + gbrands
	image_names = gimage_names # just to be consistent with previous assignment statements
	if deep:
		brands = getDeepBrands(b, urls)

	# except:
	# 	print("Unexpected error:", sys.exc_info()[0])
	# 	print(publisherURL +' not loaded successfully.')
	# 	return (False,False)

	return urls, brands, image_names

def getDeepBrands(b, urls):
	brands = []
	for i in range(len(urls)):
		if urls.index(urls[i])<i: # if the url is found earlier in the list of urls, don't open it again
			brands = brands + [brands[urls.index(urls[i])]] # just use the last brand
		else:
			b.get(urls[i])
			match = re.search(r'http[s]?://.*?/', b.current_url)
			brands = brands + [match.group() if match else '']
	return brands

def grabBrandNamesOfTaboolaAdvertisers(b):
	tabooladivs = b.find_elements_by_css_selector('div[data-item-thumb*="taboolasyndication"]')
	linkrefs = []
	for tdiv in tabooladivs:
		links = tdiv.find_elements_by_css_selector('a[href]')
		linkrefs = linkrefs + [l.get_attribute('href') for l in links]
	urls = []
	brands = []
	for link in linkrefs:
		urls = urls + [link]
		if not deep:
			match = re.search(r'http[s]?://.*?/', link)
			brands = brands + [match.group() if match else '']
	return urls, brands

	# matches = re.findall(r'(<span class="branding">)([\w \d \s .]*)(<\/span>)', b.page_source)
	# for match in matches:
	# 	print(match[1])

def grabDomainsAndImagesOfGoogleAdvertisers(b, path_to_save_files):
	print("Start google grab domain")
	viewport_height = browser.execute_script("return window.innerHeight")
	banner_height = viewport_height / 4
	b.switch_to_default_content()
	linkrefs = []
	image_names = []
	div_elems = b.find_elements_by_xpath('//div[contains(@id, "google_ads_iframe")]')
	# retrieve all the linkrefs, create associated images in file sys, and append image_name to list
	for index, div_elem in enumerate(div_elems):
		location = div_elem.location
		frame_size = div_elem.size
		#open screenshot and get size
		im = Image.open(path_to_save_files +'/screenshot.png') # uses PIL library to open image in memory
		image_width, image_height = im.size
		if location['y'] < banner_height: # banner image
			# get parameters for cropping image
			left = max(0, location['x'] - 200)
			right =  min(image_width, location['x'] + frame_size['width'] + 200)
			top = 0
			bottom = location['y'] + frame_size['height'] + 100
		else: # body page image
			left = 0
			right = image_width
			top = max(0, location['y'] - 100)
			bottom = min(image_height, location['y'] + frame_size['height'] + 10)
		# crop image
		im = im.crop((left, top, right, bottom)) # defines crop points
		image_name = path_to_save_files + '/pic' + str(index) + '.png'
		im.save(image_name,optimize=True,quality=45)
		print("\nfinish creating image and saved it to: " + image_name)

		#open new picture, and base64 encode it into a string
		googleframe = div_elem.find_element_by_tag_name('iframe')
		b.switch_to.frame(googleframe)
		possible_attrs = ['data-original-click-url','href']
		found_linkref = False
		for attr in possible_attrs:
			links = b.find_elements_by_css_selector('a[' + attr + ']')
			if len(links) == 0:
				continue
			curLength = len(linkrefs)
			print("curLength: " + str(curLength))
			linkrefs = linkrefs + [links[0].get_attribute(attr)] # in case where iframe has several links, just take first one
			print("newLength: " + str(len(linkrefs)))
			if curLength != len(linkrefs): # if linkrefs increased in size
				print("Found a linkref for image: " + image_name + " with attr: " + attr)
				image_names.append(image_name)
				print("image size: "+ str(len(image_names)))
				found_linkref = True
				break
		if not found_linkref:
			print("Could not find linkref for image: " + image_name)
		b.switch_to_default_content()
	urls = []
	brands = []
	for oneLink in linkrefs:
		urls.append(oneLink)
		if not deep:
			match = re.search(r'=http[s]?://.*?/', oneLink) # NOTE this has an extra = because the href also has a google address, it is then removed by the [1:]
			brands = brands + [match.group()[1:] if match else '']
	return urls, brands, image_names


# will randomly scrape URL listed in "publisherURLs" "numHits" times
def scrapePublisher(publisherURLs, numHits):
	# 2 seconds per ad if deep = False
	# 12 sconds per ad if deep = True

	for i in range(numHits):
		publisherURL = random.choice(publisherURLs)
		browser.get(publisherURL)
		try: # remove a div popup element that appears on the page
			browser.find_element_by_xpath('//*[@id="isc_front_takeover_close"]').click()
			time.sleep(1.0)
		except:
			print("no div popup with specified xpath: //*[@id='isc_front_takeover_close']")

		currentDatetime = str(datetime.datetime.now().isoformat())
		currentDatetime = currentDatetime[2:-6]	# trim leading two digits of year and decimals of second 
		exclude = set(string.punctuation)
		strippedDatetime = ''.join(ch for ch in currentDatetime if ch not in exclude)

		path_to_save_files = script_path+'/'+strippedDatetime
		if not os.path.isdir(path_to_save_files):
		    os.mkdir(path_to_save_files)

		urls, brands, image_names = findAndParseAdsAndImages(browser, path_to_save_files)

		csv_name = path_to_save_files +'/'+ str(int(i/14))+'_breitbart.csv'

	#	csv_name = save_path + str(int(i/14))+'_breitbart.csv'
		print(csv_name)
		# opening the file with w+ mode truncates the file (make sure we start with empty file)
		f = open(csv_name, "w+")
		f.close()

		with open(csv_name, 'a') as csvfile:
			writer = csv.writer(csvfile, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
			for j in range(len(urls)):
				print("making row" + str(j))
				with open(image_names[j], "rb") as image_file:
					image_string = image_file.read()
				writer.writerow([publisherURL, urls[j], brands[j], image_string])

		print("Starting POST request to API strike ")
		c = pycurl.Curl()
		#c.setopt(c.POST, 1)
		c.setopt(c.URL, "http://api.adstrike.us/v1/ingest?auth=7c0eb6c2fdc857b040d212e607998a6c")
	#	csv_name = "./" + csv_name

		c.setopt(c.HTTPPOST, [("file", (c.FORM_FILE, csv_name))])
		c.perform()
		c.close()
		print("Finish POST request to API strike ")

##############
## MAIN STARTS HERE
##############

script_path = os.path.dirname(os.path.abspath('__file__'))
browser = webdriver.Chrome(executable_path = script_path + '/chromedriver')

#dcap = dict(DesiredCapabilities.PHANTOMJS)
#dcap["phantomjs.page.settings.userAgent"] = (
#    "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) AppleWebKit/537.36 "
#    "(KHTML, like Gecko) Chrome/54.0.2840.98 Safari/537.36"
#) # got user-agent from running Chrome and visiting http://www.whoishostingthis.com/tools/user-agent/

#browser = webdriver.PhantomJS(executable_path = script_path + '/phantomjs', desired_capabilities=dcap)


publisherURLs = ["http://www.breitbart.com/"
#	, "http://www.breitbart.com/big-journalism/"
#	, "http://www.breitbart.com/big-hollywood/"
#	, "http://www.breitbart.com/national-security/"
#	, "http://www.breitbart.com/tech/"
#	, "http://www.breitbart.com/video/"
#	, "http://www.breitbart.com/sports/"
#	, "http://www.breitbart.com/news/"
#	, "http://www.breitbart.com/2016-presidential-race/"
	]
deep = False #if i%7 else True

while True:
	scrapePublisher(publisherURLs, 25)
	time.sleep(60*60)
	
browser.quit()
