#!/bin/sh

export DEBIAN_FRONTEND=noninteractive

apt-get install software-properties-common
apt-key adv --recv-keys --keyserver hkp://keyserver.ubuntu.com:80 0xcbcb082a1bb943db
add-apt-repository 'deb http://ftp.osuosl.org/pub/mariadb/repo/10.0/ubuntu trusty main'
apt-get update

apt-get -y install mariadb-server
apt-get -y install mariadb-client

sed -i.bkp '/bind-address/s/^/# /' /etc/mysql/my.cnf

mysql -u root -e "create user 'user'@'%' identified by 'pass'"
mysql -u root -e "create database reboot"
mysql -u root -e "grant all privileges on reboot.* to 'user'@'%'"

service mysql restart