#!/bin/sh

export DEBIAN_FRONTEND=noninteractive

apt-get update
yes | apt-get install nginx
service nginx start
yes | apt-get install php5-mysql
apt-get install php5-intl
yes | apt-get install php5-fpm
yes | apt-get install php5-cli
yes | apt-get install git
yes | apt-get install phpunit
yes | apt-get install imagemagick php5-imagick
yes | apt-get install ghostscript
yes | apt-get install nmap
apt-get install php5-curl php5-sqlite

usermod -a -G vagrant www-data

curl -sL https://deb.nodesource.com/setup | sudo bash -
yes | apt-get install nodejs
yes | apt-get install build-essential

npm install bower -g
npm install grunt -g
npm install grunt-cli -g

rm -rf /etc/nginx/sites-enabled/default
cp /vagrant/templates/api.conf /etc/nginx/sites-available/api
ln -s /etc/nginx/sites-available/api /etc/nginx/sites-enabled/api

mkdir -p /var/log/reboot/api
touch /var/log/reboot/api/error.log
touch /var/log/reboot/api/access.log

rm -rf /etc/php5/fpm/php.ini
cp /vagrant/templates/php.ini /etc/php5/fpm/php.ini
cp /vagrant/templates/xdebug.ini /etc/php5/mods-available/xdebug.ini

rm -rf /etc/nginx/nginx.conf
cp /vagrant/templates/nginx.conf /etc/nginx/nginx.conf
sed -i.bkp 's/;date.timezone =/date.timezone = America\/Denver/' /etc/php5/fpm/php.ini

sed -i.bkp 's/127.0.0.1 api.reboot.com local.api.reboot.com' /etc/hosts

service nginx restart
service php5-fpm restart

curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

composer config -g --unset github-oauth.api.github.com
composer config -g github-oauth.github.com 62e097327534909ea6dcbfd51f135602333ece6d