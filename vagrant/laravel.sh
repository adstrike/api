#!/bin/sh

cp /vagrant/templates/.env.dev /var/www/api/.env

mkdir -p /var/www/api/bootstrap/cache

chmod 777 -R /var/www/api/bootstrap/cache
chmod 777 -R /var/www/api/storage

cd /var/www/api
composer install --ignore-platform-reqs

php /var/www/api/artisan key:generate
php /var/www/api/artisan migrate
php /var/www/api/artisan db:seed
#php /var/www/api/artisan jwt:generate