# Setup:

`pip install -U selenium`
--or--
`python3 -m pip install -U selenium`

`pip install Pillow`
 --or--
 `python3 -m pip install Pillow`

## debuggers reference-- code below good for debugging large pieces of output in python terminal--
```python
sys.stdout = open("c:\\goat.txt", "w")
print(browser.page_source)
```
# How to run
Go to the scraper directory and run selenium_breitbart.py
```
cd scraper
python selenium_breitbart.py
```
you may need to run unset PYTHONPATH.

## Intermediary files that get created in the process
```
screenshot.png
image#.png
#_breitbart.csv
```
You can change these names in the code to customize the name of the output.

## Details/assumptions relating to full screen capture

Selenium is not able to capture the whole page at once. Instead, Selenium scrolls down the page by page steps, takes an ss of the users viewport, and proceeds to patch it into one big image file. So it mimics the way a user would manually capture the whole page.

Potential problems: Ads might pop up on the side along the way, shifting the ss.
Ads windows/modals may pop up.

Currently, the code deals with one type of ad that appeared on the left side of the screen and shifted everything to the right.

`print("Scrolled To ({0},{1})".format(500, rectangle[1]))`

The hack is in the 500 .The 500 is a horizontal coordinate asking Selenium to scroll to the right side of the page. This way we still get to see the rightmost side of the webpage.


# Details/assumptions relating to scraping the ads:

Places to possibly edit the scraping aspect of the code to deal with different types of ads:

1.We look for ads by looking for divs with an id that has googles_ads_iframe contained in it, and then we look for any iframes within this div:
```python
div_elems = b.find_elements_by_xpath('//div[contains(@id, "google_ads_iframe")]')
...
googleframe = div_elem.find_element_by_tag_name('iframe')
```

2.Currently in each iframe, we assume the data we want lives in
'data-original-click-url', or 'href'. Otherwise we say there is no data for that image
```python
possible_attrs = ['data-original-click-url','href']
```

3Q.What happens if there is a div modal that pops up and obscures the whole website upon visit?

3A. Find the xpath of the close button on that element. An example is shown below for a specific element found on the website.

```python
browser.find_element_by_xpath('//*[@id="isc_front_takeover_close"]').click()
time.sleep(1.0)
```

4. We assume that each ad has one significant link associated with it. But in reality, an iframe can contain a bunch of mini images with many links associated with it. (ex. a bunch of purse images lined up)

We only want work with one link, so I simplified the problem by just extracting the first url in the list.

```python
linkrefs = linkrefs + [links[0].get_attribute(attr)] # in case where iframe contains several links, just take first one
```

5. We are only looking at breitbart. If you want to change the website, change the URL in the code.

6. Image retrieval: Images on the top of the page have a more vertical crop, while other images have a more horizontal crop.

```python
viewport_height = browser.execute_script("return window.innerHeight")
banner_height = viewport_height / 4
...

if location['y'] < banner_height: # banner image
	# get parameters for cropping image
	left = max(0, location['x'] - 200)
	right =  min(image_width, location['x'] + frame_size['width'] + 200)
	top = 0
	bottom = location['y'] + frame_size['height'] + 100
else: # body page image
	left = 0
	right = image_width
	top = max(0, location['y'] - 10)
	bottom = min(image_height, location['y'] + frame_size['height'] + 10)
```

7. Anything else you want to add to deal with any new unexpected problems that appear to the everchanging website.